namespace AvaloniaApplication2.Environment;

public enum Platform : byte
{
    Linux,
    Windows,
    Unknown
}