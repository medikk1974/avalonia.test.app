namespace AvaloniaApplication2.Environment;

public interface IPlatformService
{
    Platform GetPlatform();
}