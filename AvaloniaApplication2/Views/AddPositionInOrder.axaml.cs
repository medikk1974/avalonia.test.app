﻿using System.Collections.Generic;
using Avalonia;
using Avalonia.Controls;
using Avalonia.Markup.Xaml;
using AvaloniaApplication2.ViewModels;

namespace AvaloniaApplication2.Views;

public partial class AddPositionInOrder : UserControl
{

    public AddPositionInOrder()
    {
        InitializeComponent();
        this.DataContext = new AddPositionInOrderViewModel();
    }

    private void InitializeComponent()
    {
        AvaloniaXamlLoader.Load(this);
    }
}