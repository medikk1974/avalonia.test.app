using Avalonia;
using Avalonia.Controls;
using Avalonia.Markup.Xaml;
using Avalonia.ReactiveUI;
using AvaloniaApplication2.ViewModels;
using ReactiveUI;

namespace AvaloniaApplication2.Views;

public partial class SignInWindow :  ReactiveWindow<SignInWindowViewModel>
{
    public SignInWindow()
    {
        this.WhenActivated(disposables => { });
        AvaloniaXamlLoader.Load(this);
    }
    
    
//     public SignInWindow()
//     {
//         InitializeComponent();
// #if DEBUG
//         this.AttachDevTools();
// #endif
//     }
//
//     private void InitializeComponent()
//     {
//         AvaloniaXamlLoader.Load(this);
//     }
}