using Avalonia;
using Avalonia.Controls;
using Avalonia.Markup.Xaml;
using Avalonia.ReactiveUI;
using AvaloniaApplication2.ViewModels;
using ReactiveUI;

namespace AvaloniaApplication2.Views;

public partial class SignInBarCode : ReactiveUserControl<SignInBarCodeViewModel>
{
    public SignInBarCode()
    {
        this.WhenActivated(disposables => { });
        AvaloniaXamlLoader.Load(this);
    }
}