using System;
using System.Reactive;
using ReactiveUI;
using ReactiveUI.Fody.Helpers;

namespace AvaloniaApplication2.ViewModels;

public class SignInBarCodeViewModel: ReactiveObject, IRoutableViewModel
{
    public IScreen HostScreen { get; }

    public string UrlPathSegment => nameof(SignInBarCodeViewModel);
    public ReactiveCommand<Unit, IRoutableViewModel> Login { get; }
    public ReactiveCommand<string, Unit> AddSymbolToBarCode { get; }
    public ReactiveCommand<Unit, Unit> ClearSymbolFromBarCode { get; }

    [Reactive]
    public string BarCode { get; set; }
    
    public SignInBarCodeViewModel(IScreen screen)
    {
        HostScreen = screen;
        
        IObservable<bool> canGotToInputPinCode = this.WhenAnyValue(x => x.BarCode, (barcode) =>
        {
            return (!string.IsNullOrWhiteSpace(barcode) && barcode.Length >= 10);
        });
        
        AddSymbolToBarCode = ReactiveCommand.Create<string>(symbol => AppendToBarCode(symbol));

        ClearSymbolFromBarCode = ReactiveCommand.Create(ClearSymbolFromBarcode);


        Login = ReactiveCommand.CreateFromObservable(
            execute: () => HostScreen.Router.Navigate.Execute(new SignInPinCodeViewModel(this.HostScreen)),
            canExecute: canGotToInputPinCode);
    }
    
    private void AppendToBarCode(string symbol)
    {
        BarCode += symbol;
    }

    private void ClearSymbolFromBarcode()
    {
        if (!string.IsNullOrEmpty(BarCode))
        {
            BarCode = BarCode.Substring(0, BarCode.Length - 1);
        }
    } 
 
}
