using System;
using System.Collections.ObjectModel;
using System.Reactive;
using System.Threading.Tasks;
using ReactiveUI;
using ReactiveUI.Fody.Helpers;
using ReactiveUI.Fody.Helpers;

namespace AvaloniaApplication2.ViewModels;

public class SignInPinCodeViewModel: ReactiveObject, IRoutableViewModel
{ 
    public IScreen HostScreen { get; }
    
    public string UrlPathSegment => nameof(SignInPinCodeViewModel);
    
    public ReactiveCommand<Unit, Unit> Login { get; }
    
    [Reactive]
    public ObservableCollection<string> PinCode { get; set; }

    public ReactiveCommand<string, Unit> AddSymbolToBarCode { get; }
    public ReactiveCommand<Unit, Unit> ClearSymbolFromBarCode { get; }

    public SignInPinCodeViewModel(IScreen screen)
    {
        HostScreen = screen;

        AddSymbolToBarCode = ReactiveCommand.Create<string>(symbol => AppendToBarCode(symbol));

        ClearSymbolFromBarCode = ReactiveCommand.Create(ClearSymbolFromBarcode);
        PinCode = new ObservableCollection<string>(new[] { "","","","" });

        // IObservable<bool> canGotToInputPinCode = this.WhenAnyValue(x => x.BarCode, (barcode) =>
        // {
        //     return barcode.Length > 10;
        // });

        // Login = ReactiveCommand.CreateFromTask(
        //     execute: () => Task.Delay(TimeSpan.FromSeconds(1)),
        //     canExecute: canGotToInputPinCode);

    }
    
    private void AppendToBarCode(string symbol)
    {
       // pinCode += symbol;
    }

    private void ClearSymbolFromBarcode()
    {
        // if (!string.IsNullOrEmpty(pinCode))
        // {
        //     pinCode = pinCode.Substring(0, pinCode.Length - 1);
        // }
    } 
    
    
}


public class PinCode : ReactiveObject
{
    public string Element { get; set; }
}