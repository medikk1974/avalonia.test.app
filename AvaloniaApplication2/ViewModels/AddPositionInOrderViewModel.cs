﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using AvaloniaApplication2.Models;

namespace AvaloniaApplication2.ViewModels;

public class AddPositionInOrderViewModel : ViewModelBase
{
    public ObservableCollection<Person> People { get; }
    
    public ObservableCollection<Product> Products { get; set; }

    public AddPositionInOrderViewModel()
    {
        People = new ObservableCollection<Person>(GenerateMockPeopleTable());

        Products = new ObservableCollection<Product>()
        {
            new Product()
            {
                Articul = 1,
                Name = "Flint сухарики з хріном",
                Price = 15,
                BarCods = new List<string>() { "000001" },
                DiscountPercentage = 0,
                GroupId = 1,
                PictureUrl =
                    "https://content.fora.ua/sku/ecommerce/70/480x480fora/705238_480x480fora_829f54dd-d704-ba9a-8f54-1031c4326c92.png",
                IsAgeConstraints = false,
                IsWeightProduct = false,
                RequireExciseScan = false,
                RequireExciseAdditionalScan = false,
                Quantity = 1,
                Unit = "шт"
            },
            new Product()
            {
                Articul = 2,
                Name = "Flint сухарики з холодцем",
                Price = 20,
                BarCods = new List<string>() { "000002" },
                DiscountPercentage = 5,
                GroupId = 1,
                PictureUrl =
                    "https://content.fora.ua/sku/ecommerce/70/480x480fora/705238_480x480fora_829f54dd-d704-ba9a-8f54-1031c4326c92.png",
                IsAgeConstraints = false,
                IsWeightProduct = false,
                RequireExciseScan = false,
                RequireExciseAdditionalScan = false,
                Quantity = 300,
                Unit = "гр"
            },

            new Product()
            {
                Articul = 3,
                Name = "Пиво Desperados",
                Price = 15,
                BarCods = new List<string>() { "000021" },
                DiscountPercentage = 0,
                GroupId = 2,
                PictureUrl =
                    "https://content.fora.ua/sku/ecommerce/1/480x480fora/16507_480x480fora_c65b18ff-5128-9fc9-8e67-ab890592f03d.png",
                IsAgeConstraints = true,
                IsWeightProduct = false,
                RequireExciseScan = false,
                RequireExciseAdditionalScan = false,
                Quantity = 300,
                Unit = "гр"
            },

            new Product()
            {
                Articul = 4,
                Name = "Лікер Jack Daniels Honey 35%",
                Price = 20,
                BarCods = new List<string>() { "000030" },
                DiscountPercentage = 5,
                GroupId = 3,
                PictureUrl =
                    "https://content.fora.ua/sku/ecommerce/61/480x480fora/610893_480x480fora_b84a0ded-795d-3781-b645-6f2cd99686e9.png",
                IsAgeConstraints = true,
                IsWeightProduct = false,
                RequireExciseScan = true,
                RequireExciseAdditionalScan = true,
                Quantity = 300,
                Unit = "гр"
            },

            new Product()
            {
                Articul = 5,
                Name = "Сигарети Прима Срібна червона з фільтром",
                Price = 20,
                BarCods = new List<string>() { "000040" },
                DiscountPercentage = 0,
                GroupId = 4,
                PictureUrl =
                    "https://content.fora.ua/sku/ecommerce/90/480x480fora/907522_480x480fora_569eb790-21b0-316c-94e7-905ac4022f87.png",
                IsAgeConstraints = true,
                IsWeightProduct = false,
                RequireExciseScan = true,
                RequireExciseAdditionalScan = false,
                Quantity = 300,
                Unit = "гр"
            },

            new Product()
            {
                Articul = 6,
                Name = "Печиво Delicia «Супер-Моніка» здобне",
                Price = 20,
                BarCods = new List<string>() { "000004" },
                DiscountPercentage = 0,
                GroupId = 1,
                PictureUrl =
                    "https://content.fora.ua/sku/ecommerce/86/480x480fora/869437_480x480fora_044135c5-b041-70d2-2b88-e0825d915aa0.png",
                IsAgeConstraints = false,
                IsWeightProduct = true,
                RequireExciseScan = true,
                RequireExciseAdditionalScan = false,
                Quantity = 1,
                Unit = "гр"
            },

            new Product()
            {
                Articul = 6,
                Name = "Печиво Delicia «Супер-Моніка» здобне",
                Price = 20,
                BarCods = new List<string>() { "000004" },
                DiscountPercentage = 0,
                GroupId = 1,
                PictureUrl =
                    "https://content.fora.ua/sku/ecommerce/86/480x480fora/869437_480x480fora_044135c5-b041-70d2-2b88-e0825d915aa0.png",
                IsAgeConstraints = false,
                IsWeightProduct = true,
                RequireExciseScan = true,
                RequireExciseAdditionalScan = false,
                Quantity = 1,
                Unit = "гр"
            },

            new Product()
            {
                Articul = 6,
                Name = "Печиво Delicia «Супер-Моніка» здобне",
                Price = 20,
                BarCods = new List<string>() { "000004" },
                DiscountPercentage = 0,
                GroupId = 4,
                PictureUrl =
                    "https://content.fora.ua/sku/ecommerce/86/480x480fora/869437_480x480fora_044135c5-b041-70d2-2b88-e0825d915aa0.png",
                IsAgeConstraints = false,
                IsWeightProduct = true,
                RequireExciseScan = true,
                RequireExciseAdditionalScan = false,
                Quantity = 1,
                Unit = "гр"
            }
        };
    }

    private IEnumerable<Person> GenerateMockPeopleTable()
    {
        var defaultPeople = new ObservableCollection<Person>()
        {
            new Person()
            {
                FirstName = "Pat", 
                LastName = "Patterson", 
                EmployeeNumber = 1010,
                DepartmentNumber = 100, 
                DeskLocation = "B3F3R5T7"
            },
            new Person()
            {
                FirstName = "Jean", 
                LastName = "Jones", 
                EmployeeNumber = 973,
                DepartmentNumber = 200, 
                DeskLocation = "B1F1R2T3"
            },
            new Person()
            {
                FirstName = "Terry", 
                LastName = "Tompson", 
                EmployeeNumber = 300,
                DepartmentNumber = 100, 
                DeskLocation = "B3F2R10T1"
            },
            new Person()
            {
                FirstName = "Jean", 
                LastName = "Jones", 
                EmployeeNumber = 973,
                DepartmentNumber = 200, 
                DeskLocation = "B1F1R2T3"
            },
            new Person()
            {
                FirstName = "Terry", 
                LastName = "Tompson", 
                EmployeeNumber = 300,
                DepartmentNumber = 100, 
                DeskLocation = "B3F2R10T1"
            },
            new Person()
            {
                FirstName = "Jean", 
                LastName = "Jones", 
                EmployeeNumber = 973,
                DepartmentNumber = 200, 
                DeskLocation = "B1F1R2T3"
            },
            new Person()
            {
                FirstName = "Terry", 
                LastName = "Tompson", 
                EmployeeNumber = 300,
                DepartmentNumber = 100, 
                DeskLocation = "B3F2R10T1"
            },
            new Person()
            {
                FirstName = "Jean", 
                LastName = "Jones", 
                EmployeeNumber = 973,
                DepartmentNumber = 200, 
                DeskLocation = "B1F1R2T3"
            },
            new Person()
            {
                FirstName = "Terry", 
                LastName = "Tompson", 
                EmployeeNumber = 300,
                DepartmentNumber = 100, 
                DeskLocation = "B3F2R10T1"
            },
            new Person()
            {
                FirstName = "Jean", 
                LastName = "Jones", 
                EmployeeNumber = 973,
                DepartmentNumber = 200, 
                DeskLocation = "B1F1R2T3"
            },
            new Person()
            {
                FirstName = "Terry", 
                LastName = "Tompson", 
                EmployeeNumber = 300,
                DepartmentNumber = 100, 
                DeskLocation = "B3F2R10T1"
            }
        };

        return defaultPeople;
    }
}
