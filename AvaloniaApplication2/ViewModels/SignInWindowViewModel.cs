using System.Reactive;
using ReactiveUI;
using Splat;

namespace AvaloniaApplication2.ViewModels;

public class SignInWindowViewModel : ReactiveObject, IScreen
{
    public RoutingState Router { get; } = new RoutingState();

    // The command that navigates a user to first view model.
    public ReactiveCommand<Unit, IRoutableViewModel> GoNext { get; }
    
    // The command that navigates a user back.
    public ReactiveCommand<Unit, IRoutableViewModel?> GoBack => Router.NavigateBack;

    public SignInWindowViewModel()
    {
        // GoNext = ReactiveCommand.CreateFromObservable(
        //     () => Router.Navigate.Execute(new SignInPinCodeViewModel(this))
        // );

        // Router.Navigate.Execute(new SignInPinCodeViewModel(this));
        //
        // Locator.CurrentMutable.Register(()=> Router.Navigate.Execute(new SignInPinCodeViewModel(this )));
        
        Router.Navigate.Execute(new SignInBarCodeViewModel(this));
        
        Locator.CurrentMutable.Register(()=> Router.Navigate.Execute(new SignInBarCodeViewModel(this )));
    }
}