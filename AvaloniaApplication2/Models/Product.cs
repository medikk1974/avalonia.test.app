﻿using System;
using System.Collections.Generic;

namespace AvaloniaApplication2.Models;

public class Product
{
    public int Articul { get; set; }
    
    public string Name { get; set; } 

    public List<string> BarCods { get; set; } 
    
    public Decimal Price { get; set; }
    
    public string PictureUrl { get; set; }
    
    public int GroupId { get; set; }
    
    public bool IsWeightProduct { get; set; }
    
    public bool IsAgeConstraints { get; set; }
    public bool RequireExciseScan { get; set; }
    public bool RequireExciseAdditionalScan { get; set; }
    public int DiscountPercentage { get; set; } 
    
    public int Quantity { get; set; }
    
    public string Unit { get; set; }
}