﻿namespace AvaloniaApplication2.Models;


public class Person 
{
    public int DepartmentNumber { get; set; }
        
    public string DeskLocation{ get; set; }
        
    public int EmployeeNumber { get; set; }

    public string FirstName { get; set; }
        
    public string LastName { get; set; }

    public Person(int departmentNumber, string deskLocation, int employeeNumber, string firstName, string lastName)
    {
        DepartmentNumber = departmentNumber;
        DeskLocation = deskLocation;
        EmployeeNumber = employeeNumber;
        FirstName = firstName;
        LastName = lastName;
    }

    public Person()
    {
        
    }
}
