using System;
using AvaloniaApplication2.ViewModels;
using AvaloniaApplication2.Views;
using ReactiveUI;

namespace AvaloniaApplication2;

public class AppViewLocator : ReactiveUI.IViewLocator
{
    public IViewFor ResolveView<T>(T viewModel, string contract = null) => viewModel switch
    {
        SignInPinCodeViewModel context => new SignInPinCode { DataContext = context },
        SignInBarCodeViewModel anotherContext => new SignInBarCode() { DataContext = anotherContext },
        _ => throw new ArgumentOutOfRangeException(nameof(viewModel))
    };
}